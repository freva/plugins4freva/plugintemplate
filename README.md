# Template for Freva Plugins

This template helps you to turn your data analysis code into a standalone freva plugin. This template helps you creating a [conda environment](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#id2) for your plugin as well as building and installing your plugin.

Below are the steps you'll have to do to turn your code into a plugin

1. Add your dependencies (e.g. r-rnetcdf etc) to the `deployment/plugin-env.yml` file. More information on anaconda [environment files](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#creating-an-environment-from-an-environment-yml-file) can be found here. You can search for packages on [anaconda.org](https://anaconda.org).
2. Only if needed: If you have source code in a compiled language such as C++ you should adjust the build step in the [Makefile] accordingly
3. To setup your plugin environment execute a `make all`
4. Create the wrapper API file

## Helper tools to build additional resources.
To facilitate building additional package resources in the preferred language
we have created some command line tools for installing dependencies. Currently
supported (other than python) are:
- gnu-r

To install additional packages in a supported language add a call of the
cli: `python deployment/install_resources.py` to the `build` section in the
Makefile. For example:

```
python deployment/install_resources.py gnu-r ncdf4.helpers
```
Please refer to the help menu (`python deployment/install_resources.py (<sub-command> --help`) for more information.
